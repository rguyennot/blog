# Hugo blog + Docker + GitLab CI/CD 🔥

This is an `How To` build a static site with `Markdown` + `Hugo` (**hugo-clarity** responsive theme) and `gitlab-ci`.  

## How To 🔍
* Fork the project and clone it
* `docker` + `docker-compose` 🐋 installed on your machine
* Go into your local repository and run `docker-compose up`
* Open the generated link http://localhost:1313/articles/
* Made some changes and check in your browser
* Made a git local branch and commit :)

## Organization 🔧
Every article must be name like that : `<YEAR>-<MONTH>_<SHORT-TITLE>` and content :
* Article is in `index.md`
* Assets (images, favicons, logos...)

## Hugo-Clarity features theme 🎉

* Blog with tagging and category options
* Deeplinks
* Choice of whether to use Hugo Page Bundles
* Native Image Lazy Loading
* Customizable (see config)
* Dark Mode (with UI controls for user preference setting)
* Toggleable table of contents
* Configurable Site Disclaimer (i.e. "my views are not my employer's")
* Flexible image configuration, and support for modern formats like WebP
* Logo alignment
* Mobile support with configurable menu alignment
* Syntax Highlighting
* Rich code block functions including:
    * Copy to clipboard
    * Toggle line wrap (dynamic)
    * Toggle line numbers
    * Language label
    * Toggle block expansion/contraction (dynamic)
    * To put it all in context, here is a preview showing all functionality.

## Documentation 📄

- [theme](https://github.com/chipzoller/hugo-clarity)

## License

The MIT License (MIT) - Original work Copyright (c) 2014 Steve Francia
